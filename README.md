# YetAnotherFantasyToyList
## THIS PROJECT IS WIP! Any listing that is still in bullet point format is currently unuasable!
 - Note this list will have its formatting reworked soon! Monetary amounts and star system are being reworked.

Credit for the original version of the list goes to https://twitter.com/fewhoneydew

Note for Github Staff, this repository is a master list of fantasy sex toys for educational purposes only. This repo will never include any images of any toys in or out of use. 

# Legend:

**Quality and Reviews:**
- ★’s are given to shops with exceptionally high-quality products and a history of positive reviews.
- ⛔️‘s indicate notoriously problematic shops.

**Pricing:**
- $'s indicate expense in USD (Note: Pricing may not be accurate). 
  - $: Generally cheaper than $75.
  - $$: Generally priced between $75 and $150.
  - $$$: Generally priced between $150 and $225.
  - $$$$: Generally priced higher than $225.

**Product Categories:**
- 🍩 's indicate shops that sell full-sized penetrables.
- 🥯's indicate mini penetrables.
- 🍖's indicate wearables and/or cock rings.
- 🥚's indicate eggs or kegel toys.
- 🍡’s indicate ovipositors.
- ⛲'s indicate toys with cum tubes.
- 🎈's indicate toys with inflatable knots.
- 🍆’s indicate packers.
- 💗's indicate shops that have dual-density toys available.

**Additional Notes:**
- “‡” after a shop name indicates not all their products are made with 100% body-safe silicone. Some toys may have sterilization issues.
- “↺” indicates a shop that sells knockoffs of existing toys.
- “*” indicates a note at the bottom of the list.


# Masterlist

### 1A Toys - $ - Germany  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[Website](https://shop.1atoys.net/)

### Akifu Toys - $$$ - Germany  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[Website](https://akifutoys.com/home.php) - [Etsy](https://www.etsy.com/shop/AkifuToys) - [Twitter](https://twitter.com/akifu_toys)

### A Krow’s Nest - $$ - US  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[Website](https://akrowsnest.com/) - [Etsy](https://www.etsy.com/shop/AKrowsNest)

### All-Night Toys - $$ - US  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[Website](https://allnight.toys/) - [Twitter](https://twitter.com/allnighttoys)  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*Description: US-based shop offering diverse toys.*

### Amazing Beasts - $ - Japan  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[Website](https://www.kanojotoys.com/advanced_search_result.php?keywords=amazing+beasts)  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*Description: Japanese toy manufacturer specializing in extraordinary creations.*

### Ambush Toys - $$ - US  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[Website](https://ambushtoys.com/) - [Twitter](https://twitter.com/ambushtoys) - [Instagram](https://www.instagram.com/ambushtoys/)  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*Description: US-based toy shop with a thrilling collection.*

### American Meat - $$ - US  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[Website](https://www.americanmeat.com/)  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*Description: Previously known as Dirty Internet Toys.*

### AO Enterprises - $$ - US  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[Etsy](https://www.etsy.com/shop/aoenterprises)  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*Description: Unique creations from the US.*

### Bad Dragon - $$ - US  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[Website](https://bad-dragon.com/) - [Twitter](https://twitter.com/bad_dragon)

Bad Wolf
   - [Website](https://badwolf.com/)
   - *Description: Polish-based toy manufacturer.*

Baphomet’s Workshop
   - [Website](https://baphometsworkshop.com/)
   - [Twitter](https://twitter.com/baphometstoyshop)
   - *Description: Canadian workshop creating distinctive toys.*

Bat Bites Art -  - US
   - [Website](https://batbitesart.com/)
   - [Twitter](https://twitter.com/batbitesart)
   - *Description: US-based artistry specializing in unique toys.*

The Beast Asylum
   - [Website](https://beastasylum.co.uk/)
   - *Description: UK-based asylum for exceptional toys.*

Bee’s Bizarre Bazaar‡
   - [Etsy](https://www.etsy.com/shop/BeesBizarreBazaar)
   - *Description: UK-based shop offering bizarre and unique items.*

Big Bad Boner
   - [Etsy](https://www.etsy.com/shop/BigBadBoner)
   - [Website](https://bigbadboner.nl/)
   - *Description: Dutch shop providing bold and unique toys.*

Blackfang Labs
   - [Etsy](https://www.etsy.com/shop/BlackfangLabs)
   - [Website](https://blackfanglabs.de/)
   - [Twitter](https://twitter.com/blackfanglabs)
   - *Description: German laboratory producing cutting-edge toys.*

Bratty Batty’s -  - US
   - [Etsy](https://www.etsy.com/shop/BrattyBattys)
   - [Twitter](https://twitter.com/brattybattys)
   - *Description: Unique creations from the US.*

Built Up Beasts -  - US
   - [Website](https://builtupbeasts.com/)
   - [Twitter](https://twitter.com/builtupbeasts)
   - *Description: US-based workshop crafting extraordinary toys.*

Canem Creations
   - [Etsy](https://www.etsy.com/shop/CanemCreations)
   - [Twitter](https://twitter.com/canemcreations)
   - *Description: UK-based creator of canine-inspired toys.*

Carnal Compendium (Formerly All For Naughty) -  - US
   - [Website](https://carnalcompendium.com/)
   - [Etsy](https://www.etsy.com/shop/CarnalCompendium)
   - [Twitter](https://twitter.com/carnalcompend)
   - *Description: US-based shop offering a compendium of naughty delights.*

Chillow Fantasy -  - US
   - [Etsy](https://www.etsy.com/shop/ChillowFantasy)
   - [Website](https://chillowfantasy.com/)
   - [Twitter](https://twitter.com/chillowfantasy)
   - *Description: US-based fantasy-themed toy creator.*

Concocktions -  - US
   - [Website](https://concocktions.com/)
   - [Twitter](https://twitter.com/concocktions)
   - *Description: US-based creator of unique concoctions.*

The Crafty Hedonist -  - US
   - [Etsy](https://www.etsy.com/shop/CraftyHedonist)
   - [Twitter](https://twitter.com/craftyhedonist)
   - *Description: US-based artisan crafting hedonistic delights.*

Creature Feature Toys -  - US
   - [Website](https://creaturefeaturetoys.com/)
   - [Twitter](https://twitter.com/creatureftoys)
   - *Description: US-based toy shop with a focus on fantastical creatures.*

Creature Toys
   - [Etsy](https://www.etsy.com/shop/CreatureToys)
   - *Description: German-based creators of creature-inspired toys.*

Cryptid Pleasures
   - [Website](https://cryptidpleasures.co.uk/)
   - [Twitter](https://twitter.com/cryptidpleasure)
   - *Description: UK-based shop exploring the pleasures of cryptids.*

Cultured Fiction Labs (Formerly Two Four Love) -  - US
   - [Website](https://culturedfictionlabs.com/)
   - [Twitter](https://twitter.com/culturedfiction)
   - *Description: US-based lab specializing in cultured and fictional creations.*

Darker Horse Designs -  - US
   - [Website](https://darkerhorsedesigns.com/)
   - *Description: US-based design studio with darker and unique creations.*

Daring Dildo Designs -  - US
   - [Website](https://daringdildodesigns.com/)
   - *Description: US-based designers creating daring and unique toys.*

Darque Path
   - [Website](https://darquepath.com/)
   - [Twitter](https://twitter.com/darquepath)
   - *Description: Australian-based path to unique and dark delights.*

Dauntless Rooster
   - [Etsy](https://www.etsy.com/shop/DauntlessRooster)
   - [Website](https://dauntlessrooster.com/)
   - *Description: Dutch creators of dauntless toys.*

Deep Fantasies
   - [Etsy](https://www.etsy.com/shop/DeepFantasies)
   - [Website](https://deepfantasies.ca/)
   - [Twitter](https://twitter.com/deepfantasies)
   - [Instagram](https://instagram.com/deepfantasies)
   - *Description: Canadian shop delving into deep fantasies.*

Desires Toys (Formerly Driving Desires) -  - US
   - [Website](https://desirestoys.com/)
   - [Twitter](https://twitter.com/desirestoys)
   - *Description: US-based shop catering to diverse desires.*

Dong Deals NY -  - US
   - [Etsy](https://www.etsy.com/shop/DongDealsNY)
   - *Description: US-based shop offering unique deals on dongs.*

Dragon’s Tide Brothel -  - US
   - [Etsy](https://www.etsy.com/shop/DragonsTideBrothel)
   - [Twitter](https://twitter.com/dragonstide)
   - *Description: US-based brothel with a dragon-inspired twist.*

Drake’s Shop
   - [Website](https://drakeshop.ru/)
   - [Twitter](https://twitter.com/drakeshop_ru)
   - *Description: Russian-based shop offering unique creations.*

Dread the Empire -  - US
   - [Website](https://dreadtheempire.com/)
   - [Twitter](https://twitter.com/dreadtheempire)
   - *Description: US-based empire of dread-inducing toys.*

Dungeons and Dildos -  - US
   - [Etsy](https://www.etsy.com/shop/DungeonsAndDildos)
   - *Description: US-based shop blending fantasy and pleasure.*

Elypse Art -  - US
   - [Website](https://elypseart.com/)
   - *Description: US-based artistry creating unique pieces.*

Erasexa↺
   - [Website](https://erasexa.ru/)
   - *Description: Russian creators of innovative toys.*

EroGeisha
   - [Etsy](https://www.etsy.com/shop/EroGeisha)
   - [Website](https://erogeisha.com/)
   - *Description: Canadian creators of geisha-inspired toys.*

Exotic-Erotics -  - US
   - [Website](https://exotic-erotics.com/)
   - [Twitter](https://twitter.com/exoticerotics)
   - [Tumblr](https://exotic-erotics.tumblr.com/)
   - *Description: US-based exotic and erotic toy manufacturer.*

Factory D -  - US
   - [Etsy](https://www.etsy.com/shop/FactoryD)
   - [Twitter](https://twitter.com/factorydtoys)
   - *Description: US-based factory producing distinct toys.*

Fantasticocks -  - US
   - [Etsy](https://www.etsy.com/shop/Fantasticocks)
   - [Twitter](https://twitter.com/fantasticocks)
   - [Instagram](https://instagram.com/fantasticocks)
   - *Description: US-based creators of fantastic cocks.*

Fantasy Dangus -  - US
   - [Etsy](https://www.etsy.com/shop/FantasyDangus)
   - *Description: US-based shop specializing in fantasy dangus.*

Fantanum Adult Toys -  - US
   - [Etsy](https://www.etsy.com/shop/FantanumAdultToys)
   - [Twitter](https://twitter.com/fantanumtoys)
   - *Description: US-based shop offering adult toys with a fantastical touch.*

Fantasy Grove -  - US
   - [Website](https://fantasygrove.com/)
   - [Etsy](https://www.etsy.com/shop/FantasyGrove)
   - [Twitter](https://twitter.com/fantasygrove)
   - [Instagram](https://instagram.com/fantasygrove)
   - *Description: US-based grove of fantasy-inspired delights.*

Faux Phallus
   - [Website](https://fauxphallus.com/)
   - [Twitter](https://twitter.com/fauxphallus)
   - *Description: Australian-based creators of faux phalluses.*

Fera Daemon
   - [Website](https://feradaemon.com/)
   - [Twitter](https://twitter.com/feradaemon)
   - [Instagram](https://instagram.com/feradaemon)
   - *Description: French workshop crafting daemon-inspired toys.*

Feralized Toys
   - [Etsy](https://www.etsy.com/shop/FeralizedToys)
   - *Description: German-based creators of feralized toys.*

Fetish Zone -  - US
   - [Etsy](https://www.etsy.com/shop/FetishZone)
   - *Description: US-based zone for fetish-inspired toys.*

Fiendish Toys
   - [Etsy](https://www.etsy.com/shop/FiendishToys)
   - [Twitter](https://twitter.com/fiendishtoys)
   - *Description: Canadian creators of fiendishly delightful toys.*

Fire Crotch Toys -  - US
   - [Website](https://firecrotchtoys.com/)
   - [Twitter](https://twitter.com/firecrotchtoys)
   - *Description: US-based creators of fiery delights.*

Fleshjack
   - [Website](https://fleshjack.com/)
   - *Description: Canadian manufacturer of unique pleasure products.*

Flurb!
   - [Website](https://flurb.eu/)
   - *Description: EU-based creators of flurbulent toys.*

Foxy Rabbit Toys -  - US
   - [Website](https://foxyrabbittoys.com/)
   - [Twitter](https://twitter.com/foxyrabbittoys)
   - *Description: US-based creators of foxy toys.*

Freaky Fantasies -  - US
   - [Etsy](https://www.etsy.com/shop/FreakyFantasies)
   - *Description: US-based shop specializing in freaky fantasies.*

Frisk Toys
   - [Website](https://frisktoys.com/)
   - [Twitter](https://twitter.com/frisktoys)
   - *Description: UK-based creators of frisky toys.*

Frisky Labz -  - US
   - [Etsy](https://www.etsy.com/shop/FriskyLabz)
   - [Website](https://friskylabz.com/)
   - [Twitter](https://twitter.com/friskylabz)
   - *Description: US-based lab crafting frisky delights.*

From the Edge Toys -  - US
   - [Website](https://fromtheedgetoys.com/)
   - [Twitter](https://twitter.com/fromtheedgetoys)
   - *Description: US-based creators offering toys from the edge.*

Furry Style
   - [Website](https://furrystyle.com/)
   - *Description: German-based creators of furry-style toys.*

Gaudy Gryphon‡
   - [Website](https://gaudygryphon.com/)
   - [Etsy](https://www.etsy.com/shop/GaudyGryphon)
   - [Twitter](https://twitter.com/gaudygryphon)
   - [Instagram](https://instagram.com/gaudygryphon)
   - *Description: UK-based shop with gaudy and gryphon-inspired creations.*

Geeky Sex Toys‡
   - [Etsy](https://www.etsy.com/shop/GeekySexToys)
   - [Website](https://geekysextoys.com/)
   - [Twitter](https://twitter.com/geekysextoys)
   - *Description: Australian creators of geeky and unique adult toys.*

Gelatinous Creations -  - US
   - [Etsy](https://www.etsy.com/shop/GelatinousCreations)
   - [Twitter](https://twitter.com/gelatinouscreate)
   - *Description: US-based creators of gelatinous delights.*

Gorilla Machine
   - [Website](https://gorillamachine.com/)
   - [Twitter](https://twitter.com/gorillamachine)
   - *Description: Portuguese creators of machines with a gorilla twist.*

Guilty Box -  - US
   - [Etsy](https://www.etsy.com/shop/GuiltyBox)
   - *Description: US-based box of guilty pleasures.*

Happy Hole Toys -  - US
   - [Website](https://happyholetoys.com/)
   - [Twitter](https://twitter.com/happyholetoys)
   - *Description: US-based creators of toys that bring happiness.*

Happy Octopus Toys
   - [Etsy](https://www.etsy.com/shop/HappyOctopusToys)
   - *Description: UK-based creators of joyous octopus-inspired toys.*

High Fantasy Creations -  - US
   - [Etsy](https://www.etsy.com/shop/HighFantasyCreations)
   - *Description: US-based creators of high-fantasy toys.*

Hodgepodge Entourage -  - US
   - [Etsy](https://www.etsy.com/shop/HodgepodgeEntourage)
   - [Website](https://hodgepodgeentourage.com/)
   - [Twitter](https://twitter.com/hodgepodgeento)
   - *Description: US-based entourage of hodgepodge creations.*

Horny Ram Adult Toys -  - US
   - [Website](https://hornyramtoys.com/)
   - [Twitter](https://twitter.com/hornyramtoys)
   - *Description: US-based creators of adult toys with a ram-inspired twist.*

Howling Horrors -  - US
   - [Etsy](https://www.etsy.com/shop/HowlingHorrors)
   - [Twitter](https://twitter.com/howlinghorrors)
   - *Description: US-based creators of howlingly delightful toys.*

Howling Rabbit Hole -  - US
   - [Twitter](https://twitter.com/howlingrabbith)
   - [Instagram](https://instagram.com/howlingrabbithole)
   - *Description: US-based rabbit hole of howling creations. Accepting commissions.*

Hyper Realistic Dildos
   - [Etsy](https://www.etsy.com/shop/HyperRealisticDildos)
   - *Description: UK-based creators of hyper-realistic dildos.*

Inumi Toys
   - [Etsy](https://www.etsy.com/shop/InumiToys)
   - *Description: Swiss creators of unique toys.*

Ivy Toys
   - [Etsy](https://www.etsy.com/shop/IvyToys)
   - *Description: German creators of ivy-inspired toys.*

JackOToy
   - [Etsy](https://www.etsy.com/shop/JackOToy)
   - *Description: French creators of Jack O' Toys.*

John Thomas Toys↺
   - [Etsy](https://www.etsy.com/shop/JohnThomasToys)
   - *Description: UK-based creators of unique toys.*

JoyFlex Toys
   - [Etsy](https://www.etsy.com/shop/JoyFlexToys)
   - *Description: Finnish creators of joyfully flexible toys.*

Joy Stick Adult Toys -  - US
   - [Etsy](https://www.etsy.com/shop/JoyStickAdultToys)
   - *Description: US-based creators of adult toys with a joy stick twist.*

Kink Forge
   - [Website](https://kinkforge.com/)
   - *Description: UK-based forge specializing in kinkiness.*

Kisu and Friends
   - [Website](https://kisuandfriends.com/)
   - [Twitter](https://twitter.com/kisuandfriends)
   - *Description: Finnish creators of kissable toys and friends.*

Kitsune Comforts -  - US
   - [Twitter](https://twitter.com/KitsuneComforts)
   - *Description: US-based comfort creators with a kitsune twist.*

Knotty Boy Toys
   - [Etsy](https://www.etsy.com/shop/KnottyBoyToys)
   - *Description: Canadian creators of knotty delights.*

Kudu Voodoo -  - US
   - [Website](https://kuduvoodoo.com/)
   - [Twitter](https://twitter.com/kuduvoodoo)
   - *Description: US-based creators of voodoo-inspired toys.*

LewdCo -  - US
   - [Etsy](https://www.etsy.com/shop/LewdCo)
   - *Description: US-based creators of lewd delights.*

Lair of Lust -  - US
   - [Etsy](https://www.etsy.com/shop/LairofLust)
   - *Description: US-based lair of lustful creations.*

Loot Cave Toys -  - US
   - [Website](https://lootcavetoys.com/)
   - [Twitter](https://twitter.com/lootcavetoys)
   - *Description: US-based creators of treasures from the loot cave.*

Lovecrafters Toys
   - [Etsy](https://www.etsy.com/shop/LovecraftersToys)
   - [Twitter](https://twitter.com/lovecrafters)
   - *Description: Canadian creators inspired by Lovecraftian horrors.*

LoveMuscle Toys -  - US
   - [Etsy](https://www.etsy.com/shop/LoveMuscleToys)
   - [Website](https://lovemuscletoys.com/)
   - [Twitter](https://twitter.com/lovemuscletoys)
   - *Description: US-based creators of love-inducing toys.*

Love Smiths Toys
   - [Etsy](https://www.etsy.com/shop/LoveSmithsToys)
   - [Website](https://lovesmithstoys.com/)
   - [Twitter](https://twitter.com/lovesmithstoys)
   - *Description: French creators of love-smithed toys.*

Linger Tips
   - [Etsy](https://www.etsy.com/shop/LingerTips)
   - *Description: UK-based creators of lingering tips.*

Lycantasy -  - US
   - [Website](https://lycantasy.com/)
   - [Twitter](https://twitter.com/lycantasy)
   - *Description: Creators of lycanthropic fantasies.*

Lust Arts -  - US
   - [Website](https://lustarts.com/)
   - [Twitter](https://twitter.com/lustarts)
   - [Instagram](https://instagram.com/lustarts)
   - *Description: US-based creators of lustful arts.*

Made To Were -  - US
   - [Etsy](https://www.etsy.com/shop/MadeToWere)
   - [Twitter](https://twitter.com/madetowere)
   - *Description: US-based creators of transformative toys.*

MandMArtsLLC -  - US
   - [Etsy](https://www.etsy.com/shop/MandMArtsLLC)
   - *Description: US-based arts and crafts shop.*

Masterwork Toys -  - US
   - [Etsy](https://www.etsy.com/shop/MasterworkToys)
   - [Twitter](https://twitter.com/masterworktoys)
   - *Description: US-based creators of masterful toys.*

Monster Cocks -  - US
   - [Website](https://monstercocks.com/)
   - *Description: US-based creators of monstrous pleasures.*

MFA Designs -  - US
   - [Website](https://mfadesigns.com/)
   - [Twitter](https://twitter.com/mfadesigns)
   - *Description: US-based design studio with a focus on unique creations.*

Monster Smash
   - [Website](https://monstersmash.co.uk/)
   - [Twitter](https://twitter.com/monstersmashco)
   - *Description: UK-based creators of monstrous delights.*

Moon Fox Toys -  - US
   - [Website](https://moonfoxtoys.com/)
   - [Twitter](https://twitter.com/moonfoxtoys)
   - *Description: US-based creators inspired by the moon and foxes.*

Moonset Creations -  - US
   - [Etsy](https://www.etsy.com/shop/MoonsetCreations)
   - *Description: US-based creators of creations inspired by moonsets.*

Mr. Hankey’s Toys -  - US/UK
   - [Etsy](https://www.etsy.com/shop/MrHankeysToys)
   - [Website](https://mrhankeystoys.com/)
   - [Twitter](https://twitter.com/mrhankeystoys)
   - *Description: US and UK-based creators of toys that leave a lasting impression.*

My Myth Arts
   - [Website](https://mymytharts.com/)
   - [Twitter](https://twitter.com/mymytharts)
   - *Description: Chinese creators of mythical and artistic toys.*

Nameless Angel Works
   - [Etsy](https://www.etsy.com/shop/NamelessAngelWorks)
   - *Description: UK-based creators of works from the nameless realm.*

NaughtyFoxDE
   - [Website](https://naughtyfox.de/)
   - *Description: German creators of naughty delights.*

Naughty Wolf Toys
   - [Website](https://naughtywolf.toys/)
   - [Twitter](https://twitter.com/naughtywolf_toys)
   - *Description: EU-based creators of toys inspired by naughty wolves.*

Nawty Toy Box -  - US
   - [Etsy](https://www.etsy.com/shop/NawtyToyBox)
   - *Description: US-based box of nawty delights.*

Neotori
   - [Etsy](https://www.etsy.com/shop/Neotori)
   - [Etsy2](https://www.etsy.com/shop/NeotoriToo)
   - [Website](https://neotori.com/)
   - [Twitter](https://twitter.com/neotori)
   - [FurAffinity](https://furaffinity.net/user/neotori)
   - *Description: German creators of neotoric delights.*

NerdClimax‡ -  - US
   - [Etsy](https://www.etsy.com/shop/NerdClimax)
   - [Twitter](https://twitter.com/nerdclimax)
   - [Facebook](https://facebook.com/nerdclimax)
   - *Description: US-based creators of nerdy climactic experiences.*

New Folklore Silicone -  - US
   - [Etsy](https://www.etsy.com/shop/NewFolkloreSilicone)
   - [Twitter](https://twitter.com/newfolkloresili)
   - *Description: US-based creators of folklore-inspired silicone creations.*

Noodle Crafters -  - US
   - [Etsy](https://www.etsy.com/shop/NoodleCrafters)
   - [Twitter](https://twitter.com/noodlecrafters)
   - *Description: US-based crafters of noodle-inspired delights.*

Nothosaur -  - China
   - [Website](https://nothosaur.com/)
   - *Description: Chinese creators of unique creations inspired by ancient sea reptiles.*

Organotoy
   - [Website](https://organotoy.com/)
   - [Twitter](https://twitter.com/organotoy)
   - *Description: German creators of organic-inspired toys.*

Ourano Scopes
   - [Etsy](https://www.etsy.com/shop/OuranoScopes)
   - [Twitter](https://twitter.com/ouranoscopes)
   - *Description: Canadian creators of scopes into pleasure.*

Paladin Pleasure Sculptors -  - US
   - [Website](https://paladinpleasuresculptors.com/)
   - [Twitter](https://twitter.com/paladinpleasure)
   - *Description: US-based creators sculpting pleasure for paladins.*

Petit Vice
   - [Website](https://petitvice.com/)
   - *Description: French creators of petite and viced delights.*

Phoenix Flame Creations (formerly Phoenix Flame Forge) -  - US
   - [Website](https://phoenixflamecreations.com/)
   - [Etsy](https://www.etsy.com/shop/PhoenixFlameCreations)
   - [Twitter](https://twitter.com/pf_creations)
   - [Instagram](https://instagram.com/phoenixflamecreations)
   - [Facebook](https://facebook.com/phoenixflamecreations)
   - *Description: US-based creators forging creations with a phoenix's flame.*

Phreak Club
   - [Website](https://phreakclub.com/)
   - [Etsy](https://www.etsy.com/shop/PhreakClub)
   - [Twitter](https://twitter.com/phreakclub)
   - [Instagram](https://instagram.com/phreakclub)
   - [Facebook](https://facebook.com/phreakclub)
   - *Description: UK-based club of phreaky creations.*

Pink Box Toyshop (formerly Gems Little Pink Box)
   - [Etsy](https://www.etsy.com/shop/PinkBoxToyshop)
   - [Twitter](https://twitter.com/pinkboxtoyshop)
   - *Description: UK-based toyshop with a pink twist.*

Playing with Pleasure
   - [Etsy](https://www.etsy.com/shop/PlayingWithPleasure)
   - *Description: Canadian creators playing with pleasure.*

Pleasure Forge -  - US
   - [Website](https://pleasureforge.com/)
   - [Twitter](https://twitter.com/pleasureforge)
   - [Tumblr](https://pleasureforge.tumblr.com/)
   - *Description: US-based forge crafting pleasures.*

Primal Hardwere -  - US
   - [Website](https://primalhardwere.com/)
   - [Twitter](https://twitter.com/primalhardwere)
   - *Description: US-based creators of primal and hard toys.*

The Prince’s Court -  - US
   - [Website](https://theprincescourt.com/)
   - [Twitter](https://twitter.com/theprincescourt)
   - *Description: US-based court of princely pleasures.*

Pris Toys -  - US
   - [Website](https://pris.toys/)
   - [Etsy](https://www.etsy.com/shop/PrisToys)
   - [Twitter](https://twitter.com/pris_toys)
   - [Instagram](https://instagram.com/pris_toys)
   - *Description: US-based creators offering toys inspired by pleasure.*

Qimera Forge -  - US
   - [Website](https://qimeraforge.com/)
   - [Twitter](https://twitter.com/qimeraforge)
   - [Instagram](https://instagram.com/qimeraforge)
   - [FurAffinity](https://furaffinity.net/user/qimeraforge)
   - *Description: US-based forge creating qimeric delights.*

Ravenous Toys -  - US
   - [Etsy](https://www.etsy.com/shop/RavenousToys)
   - [Twitter](https://twitter.com/ravenoustoys)
   - *Description: US-based creators of toys that awaken ravenous desires.*

Rcc Creations Shop -  - US
   - [Etsy](https://www.etsy.com/shop/RccCreationsShop)
   - *Description: US-based shop offering unique creations.*

Red Panda Toys↺
   - [Website](https://redpandatoys.com/)
   - *Description: Brazilian creators of toys with a red panda twist.*

Rexalpha
   - [Website](https://rexalpha.com/)
   - [Etsy](https://www.etsy.com/shop/Rexalpha)
   - [Twitter](https://twitter.com/rexalpha)
   - *Description: UK-based creators of rexcellent toys.*

Salty Doggo -  - Canada
   - [Etsy](https://www.etsy.com/shop/SaltyDoggo)
   - [Twitter](https://twitter.com/saltydoggodotcom)
   - *Description: Creators of salty delights.*

Senmurv Studios -  - Canada
   - [Etsy](https://www.etsy.com/shop/SenmurvStudios)
   - [Twitter](https://twitter.com/senmurvstudios)
   - *Description: Creators of unique and mythical toys.*

Sidewinder Creations
   - [Website](https://sidewindercreations.com/)
   - *Description: German creators of sidewinding delights.*

SilcArts -  - US
   - [Etsy](https://www.etsy.com/shop/SilcArts)
   - [Twitter](https://twitter.com/silcarts)
   - [Instagram](https://instagram.com/silcarts)
   - [Facebook](https://facebook.com/silcarts)
   - *Description: US-based creators of artistic silicone creations.*

Sinnovator
   - [Etsy](https://www.etsy.com/shop/Sinnovator)
   - [Twitter](https://twitter.com/sinnovator)
   - [Facebook](https://facebook.com/sinnovator)
   - *Description: UK-based creators of innovative and sinfully delightful toys.*

Something Squishy Toys -  - US
   - [Website](https://somethingsquishytoys.com/)
   - [Etsy](https://www.etsy.com/shop/SomethingSquishyToys)
   - [Twitter](https://twitter.com/squishytoys)
   - *Description: US-based creators of squishy delights.*

Split Peaches -  - US
   - [Etsy](https://www.etsy.com/shop/SplitPeaches)
   - [Website](https://splitpeaches.com/)
   - [Twitter](https://twitter.com/splitpeaches)
   - *Description: US-based creators of uniquely shaped delights.*

Stiff Love -  - US
   - [Etsy](https://www.etsy.com/shop/StiffLove)
   - *Description: US-based creators of stiffly loving toys.*

Strange Bedfellas -  - US
   - [Website](https://strangebedfellas.com/)
   - [Twitter](https://twitter.com/strangebedfella)
   - [Instagram](https://instagram.com/strangebedfellas)
   - *Description: US-based creators of strangely delightful toys.*

Survey of the Cosmos -  - US
   - [Website](https://surveyofthecosmos.com/)
   - *Description: US-based surveyors of cosmic pleasures.*

Tender Monster Toys
   - [Website](https://tendermonstertoys.com/)
   - [Twitter](https://twitter.com/tendermonster)
   - *Description: Canadian creators of tender and monstrous toys.* (NEW)

Tentickle Toys
   - [Website](https://tentickletoys.com/)
   - [Etsy](https://www.etsy.com/shop/TentickleToys)
   - [Twitter](https://twitter.com/tentickletoys)
   - *Description: UK-based creators of tentacled delights.*

TopNotchDongs
   - [Etsy](https://www.etsy.com/shop/TopNotchDongs)
   - *Description: Czech creators of top-notch dongs.*

Topped Toys
   - [Website](https://toppedtoys.com/)
   - [Twitter](https://twitter.com/toppedtoys)
   - *Description: Canadian creators of toys that are on top.*

Twilight Meadow Creations
   - [Website](https://twilightmeadowcreations.com/)
   - *Description: Swedish creators of creations inspired by twilight meadows.*

Twin Tail Creations -  - US
   - [Website](https://twintailcreations.com/)
   - [Twitter](https://twitter.com/twintailcreatio)
   - [Instagram](https://instagram.com/twintailcreations)
   - [FurAffinity](https://furaffinity.net/user/twintailcreations)
   - *Description: US-based creators of twin-tailed delights.*

Twisted Beast
   - [Website](https://twistedbeast.co.uk/)
   - [Twitter](https://twitter.com/twistedbeastuk)
   - *Description: UK-based creators of delightfully twisted toys.*

Uberrime -  - US
   - [Etsy](https://www.etsy.com/shop/Uberrime)
   - [Website](https://uberrime.com/)
   - [Twitter](https://twitter.com/uberrimedildos)
   - *Description: US-based creators offering superbly crafted toys.*

Uncover Creations
   - [Etsy](https://www.etsy.com/shop/UncoverCreations)
   - [Twitter](https://twitter.com/uncovercreation)
   - [Instagram](https://instagram.com/uncovercreations)
   - *Description: UK-based creators uncovering unique creations.* (NEW)

VaaChar
   - [Website](https://vaachar.com/)
   - [Twitter](https://twitter.com/vaachar)
   - *Description: German creators of vaachar-inspired toys.*

Vagary Grove
   - [Website](https://vagarygrove.com/)
   - [Twitter](https://twitter.com/vagarygrove)
   - *Description: UK-based creators of toys that roam with vagary.*

Vaquero Toys
   - [Etsy](https://www.etsy.com/shop/VaqueroToys)
   - [Twitter](https://twitter.com/vaquerotoys)
   - [Tumblr](https://vaquerotoys.tumblr.com/)
   - *Description: Australian creators of toys with a vaquero twist.*

Vex Toy
   - [Etsy](https://www.etsy.com/shop/VexToy)
   - *Description: Canadian creators of vexing delights.*

Vulpini Design -  - US
   - [Etsy](https://www.etsy.com/shop/VulpiniDesign)
   - *Description: US-based creators of designs inspired by vulpine delights.*

Wandering Bard Toys -  - US
   - [Website](https://wanderingbardtoys.com/)
   - [Twitter](https://twitter.com/wanderingbardtoy)
   - *Description: US-based creators of toys that wander with a bard's spirit.*

Warminside Creations -  - US
   - [Etsy](https://www.etsy.com/shop/WarminsideCreations)
   - *Description: US-based creators warming up pleasure with unique creations.*

Weird Toyz Shop
   - [Etsy](https://www.etsy.com/shop/WeirdToyzShop)
   - *Description: Czech creators of weirdly delightful toys.*

Weredog
   - [Website](https://weredog.art/)
   - [Twitter](https://twitter.com/weredog_art)
   - *Description: UK-based creators of artful were-inspired toys.*

The Wicked Hunt -  - US
   - [Website](https://thewickedhunt.com/)
   - [Twitter](https://twitter.com/thewickedhunt)
   - *Description: US-based creators on a wicked hunt for pleasure.*

Wicked Vixen -  - US
   - [Etsy](https://www.etsy.com/shop/WickedVixenToys)
   - *Description: US-based creators of wickedly delightful toys.*

Wildo's Dildos (formerly Sabaist Dicks) -  - US
   - [Etsy](https://www.etsy.com/shop/WildosDildos)
   - *Description: US-based creators of wild and unique dildos.*

The Wonder Toys
   - [Etsy](https://www.etsy.com/shop/TheWonderToys)
   - *Description: Czech creators of wonderful toys.*

Wyverns Vault
   - [Etsy](https://www.etsy.com/shop/WyvernsVault)
   - [Twitter](https://twitter.com/wyvernsvault)
   - *Description: Canadian creators of vaulted treasures inspired by wyverns.*

XenoCat Artifacts -  - US
   - [Etsy](https://www.etsy.com/shop/XenoCatArtifacts)
   - [Website](https://xenocatartifacts.com/)
   - [Twitter](https://twitter.com/xenocatartifacts)
   - *Description: US-based creators of artifacts from alien cats.*

Yiff Monkey -  - US
   - [Etsy](https://www.etsy.com/shop/YiffMonkey)
   - [Website](https://yiffmonkey.com/)
   - [Twitter](https://twitter.com/yiffmonkey)
   - *Description: US-based creators of monkeying delights.*

Zetapaws‡ -  - US
   - [Website](https://zetapaws.com/)
   - *Description: US-based creators of pawsitively delightful toys.*

Zlotech
   - [Etsy](https://www.etsy.com/shop/Zlotech)
   - [Twitter](https://twitter.com/zlotechtoys)
   - *Description: Dutch creators of technological delights.*



# Notes:
 - Bee’s Bizarre Bazaar: Poor quality and quality control, toys full of craft glitter
 - Desires Toys: Long wait times, harassment of customers
 - Foxy Rabbit Toys: Extreme wait times -  customers waiting years
 - Lovecrafters Toys: Poor quality, toys contain many airbubbles
 - NerdClimax: Poor quality and quality control
 - Moon Fox Toys: Aggression towards customers, questionable safety precautions, airbubbles
 - Organotoy: Poor quality control, poor CS, long wait times, extreme aggression towards customers, unsafe techniques that lead to peeling (airbrushing)
 - Zetapaws: Silicone quality questionable
